const API_KEY = "b9b865a7a966d097e5e8073a81b39a83";
const PER_PAGE = 50;
const USER_ID = "155573498%40N02";
const COLS = 5;
let firstLoad = true;
let curPage = 1;
let arr = [];
const HEIGHT = 150;

getData();
document.onkeydown = eventHandler;

async function getData(page = curPage) {
  const request = await fetch(
    `https://www.flickr.com/services/rest/?method=flickr.favorites.getPublicList&api_key=${API_KEY}&user_id=${USER_ID}&per_page=${
      firstLoad ? PER_PAGE : PER_PAGE - 10
    }&format=json&nojsoncallback=1&extras=url_q&page=${page}`
  );

  const {
    photos: { photo: photos },
  } = await request.json();

  //the same thing

  //   const result = await request.json();
  //   const photos = result.photos.photo;
  //   const page = result.photos.page;
  if (page === 2) {
    photos.splice(0, 10);
  }
  showPhoto(photos);
}
function showPhoto(photos) {
  photos.forEach((photo) => {
    const wrap_img = document.createElement("div");
    wrap_img.className = "img-wrap";

    const img = document.createElement("img");
    img.src = photo.url_q;
    img.alt = photo.title;

    const lastIndex =
      arr.length > 0
        ? Number(arr[arr.length - 1].getAttribute("tabindex"))
        : -1;
    wrap_img.setAttribute("tabindex", lastIndex + 1);

    wrap_img.appendChild(img);
    document.querySelector(".images").appendChild(wrap_img);
    arr.push(wrap_img);
  });

  if (firstLoad) {
    firstLoad = false;
    arr[0].focus();
  } else {
    let curIndex = document.activeElement.tabIndex;
    index = Math.min(curIndex, arr.length - 1);
  }
}

function eventHandler(event) {
  let curIndex = Number(document.activeElement.tabIndex);
  switch (event.code) {
    case "ArrowLeft":
      curIndex = Math.max(curIndex - 1, 0);
      break;

    case "ArrowRight":
      curIndex = Math.min(curIndex + 1);
      break;

    case "ArrowDown":
      curIndex = Math.min(curIndex + COLS, arr.length - 1);
      scrollTo(
        document.documentElement,
        (Math.floor(curIndex / COLS) - 1) * HEIGHT,
        200
      );
      break;

    case "ArrowUp":
      curIndex = Math.max(curIndex - COLS, 0);
      scrollTo(
        document.documentElement,
        (Math.floor(curIndex / COLS) - 1) * HEIGHT,
        200
      );
      break;

    case "Tab":
      return false;
  }
  if (loadMore(curIndex)) {
    curPage++;
    getData(curPage);
  }
  arr[curIndex].focus();
}

function loadMore(curIndex) {
  const bound = arr.length - COLS * 2;
  return curIndex >= bound;
}

function scrollTo(element, to = 0, duration = 1000) {
  const start = element.scrollTop;
  const step = to - start;

  const increment = 20;
  let currentTime = 0;

  const animateScroll = () => {
    currentTime += increment;

    const value = Math.easeOutCubic(currentTime, start, step, duration);

    element.scrollTop = value;

    if (currentTime < duration) {
      setTimeout(animateScroll, increment);
    }
  };

  animateScroll();
}

Math.easeOutCubic = function (t, b, c, d) {
  t /= d;
  t--;
  return c * (t * t * t + 1) + b;
};

// t is current time
// b is start value
// c is change in value
// d is duration
// Math.easeInOutQuad = function (t, b, c, d) {
//   t /= d / 2;
//   if (t < 1) return (c / 2) * t * t + b;
//   t--;
//   return (-c / 2) * (t * (t - 2) - 1) + b;
// };
